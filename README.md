Here are the programming codes involved in a manuscript submitted to Computers & Geosciences, named as "Demonstrations on storage and parallel implementation for large sparse matrix used in tomographic inversion".
This programming codes are for a real-world example, written in Fortran 90 program language.
User would want this code to conduct some tests and also reproduce results mentioned in manuscript.
The source codes are stored in directory "src", while the example data is stored in directory "example".

-------------------
1. compile code

It is very easy to compile programming codes, user just run a script file named 'compile'.
Four executive files would be compiled by a compiler named 'gfortran'.
After compilation is completed, user will get four executable files: inver1, inver1.omp inver2, inver2.omp
All of the above files can be used to conduct this real-world example. The difference between these files is as following:

inver1:     execute without OMP parallel environment; It is used to test the runtime for matrix-vector and trans-matrix-vector multiplication just by using 1 core.
inver1.omp: execute within  OMP parallel environment; It is used to test the runtime for matrix-vector and trans-matrix-vector multiplication by using 10 cores.
inver2:     execute without OMP parallel environment; It is used to test the runtime for inversion process subroutine just by using 1 core.
inver2.omp: execute within  OMP parallel environment; It is used to test the runtime for inversion process subroutine by using 10 cores.

-------------------
2. run example
In directory of example, user can find two necessary data files named as 'Gr_ray.S.bp1.iasp91.rayinblock' and 'grid.velocity.S.equation.iasp91'.
User can run any of the four executive files to reproduce the results mentioned in manuscript. The command is as following:

../src/inver2.omp Gr_ray.S.bp1.iasp91.rayinblock grid.velocity.S.equation.iasp91 40.0

The relevant runtime would be shown out on screen.
Note, the runtime for matrix-vector and trans-matrix-vector multiplication are pushed out many times until the execute program is finished.
User should carefully distinguish these runtime.
Lastly, user should get a file named as 'try.ray.S.bp1.iasp91.rayinblock.40.0.ratio', that is the tomographic result. This file should be same as the file stored in outdir.