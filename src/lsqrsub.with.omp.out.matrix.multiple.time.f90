!Modify history
! 2016.05.27 UT AUSTIN, change lenrw type to integer(kind=8)
!               the follow variable value is changed accordingly
!               l l1 l2
!
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!     File lsqr.f90  (double precision)
!
!     LSQR     d2norm
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
subroutine LSQR                                 &
          (m,n,damp,wantse,a,ja,na,leniw,lenrw, &
          u,x,se,nabeg,naend,                   &
          atol,btol,conlim,itnlim,              &
          istop,itn,Anorm,Acond,rnorm,Arnorm,xnorm)
implicit none
logical            wantse
integer(kind=8)    lenrw,nabeg(leniw),naend(leniw)
integer            m, n, leniw, itnlim, istop, itn
integer            ja(lenrw),na(leniw)
double precision   a(lenrw),u(m),x(n),se(n)
double precision   atol, btol, conlim, damp
double precision   Anorm, Acond, rnorm, Arnorm, xnorm
character chardamp*100

!-----------------------------------------------------------------------

      external           d2norm, dnrm2, dcopy, dscal,Aprod
      double precision   d2norm, dnrm2

!     Local variables
      logical            damped, prnt
      integer            i, maxdx, nconv, nstop
      double precision   alfopt,                                   &
                         alpha, beta, bnorm, cs, cs1, cs2, ctol,   &
                         delta, dknorm, dnorm, dxk, dxmax, gamma,  &
                         gambar, phi, phibar, psi, res2, rho,      &
                         rhobar, rhbar1, rhs, rtol, sn, sn1, sn2,  &
                         t, tau, temp, test1, test2, test3, theta, &
                         t1, t2, t3, xnorm1, z, zbar
      integer::nout
      double precision::v(n),w(n)
      double precision, parameter :: zero = 0.0d+0, one = 1.0d+0
      character(len=*), parameter :: enter = ' Enter LSQR.  '
      character(len=*), parameter :: exitt = ' Exit  LSQR.  '
      character(len=*), parameter :: msg(0:5) =                   &
      (/ 'The exact solution is  x = 0                         ', &
         'A solution to Ax = b was found, given atol, btol     ', &
         'A least-squares solution was found, given atol       ', &
         'A damped least-squares solution was found, given atol', &
         'Cond(Abar) seems to be too large, given conlim       ', &
         'The iteration limit was reached                      ' /)
!     ------------------------------------------------------------------

!     Initialize.
      nout=10
      if (nout > 0) then
	 write(chardamp,*)damp;chardamp=adjustl(chardamp)
	 chardamp="LSQR."//chardamp(1:len_trim(chardamp))//".txt"
	 open(nout,file=chardamp,status='unknown')
         write(nout, 1000) enter, m, n, damp, wantse, &
                           atol, conlim, btol, itnlim
      end if

      damped =   damp > zero
      itn    =   0
      istop  =   0
      nstop  =   0
      maxdx  =   0
      ctol   =   zero
      if (conlim > zero) ctol = one / conlim
      Anorm  =   zero
      Acond  =   zero
      dnorm  =   zero
      dxmax  =   zero
      res2   =   zero
      psi    =   zero
      xnorm  =   zero
      xnorm1 =   zero
      cs2    = - one
      sn2    =   zero
      z      =   zero

!     ------------------------------------------------------------------
!     Set up the first vectors u and v for the bidiagonalization.
!     These satisfy  beta*u = b,  alpha*v = A(transpose)*u.
!     ------------------------------------------------------------------
      v(1:n) = zero
      x(1:n) = zero

      if (wantse) then
         se(1:n) = zero
      end if

      alpha  = zero
      beta   = dnrm2 (m, u, 1)

      if (beta > zero) then
         call dscal (m, (one/beta), u, 1)
         call Aprod (2, m, n, v, u, a,ja,na,leniw,lenrw,nabeg,naend)
         alpha = dnrm2 (n, v, 1)
      end if

      if (alpha > zero) then
         call dscal (n, (one/alpha), v, 1)
         call dcopy (n, v, 1, w, 1)
      end if

      Arnorm = alpha*beta
      if (Arnorm == zero) go to 800

      rhobar = alpha
      phibar = beta
      bnorm  = beta
      rnorm  = beta

      if (nout > 0) then
         if (damped) then
             write(nout,1300)
         else
             write(nout,1200)
         end if
         test1 = one
         test2 = alpha/beta
         write(nout, 1500) itn,x(1),rnorm,test1,test2
      end if


!     ==================================================================
!     Main iteration loop.
!     ==================================================================
  100 itn = itn + 1
	  
!     ------------------------------------------------------------------
!     Perform the next step of the bidiagonalization to obtain the
!     next  beta, u, alpha, v.  These satisfy the relations
!                beta*u  =  A*v  -  alpha*u,
!               alpha*v  =  A(transpose)*u  -  beta*v.
!     ------------------------------------------------------------------
      call dscal (m,(- alpha), u, 1)
      call Aprod (1, m, n, v, u, a,ja,na,leniw,lenrw,nabeg,naend)
      beta   = dnrm2 (m,u,1)

!     Accumulate  Anorm = || Bk ||
!                       =  sqrt( sum of  alpha**2 + beta**2 + damp**2 ).

      temp   = d2norm(alpha, beta)
      temp   = d2norm(temp , damp)
      Anorm  = d2norm(Anorm, temp)

      if (beta > zero) then
         call dscal (m, (one/beta), u, 1)
         call dscal (n, (- beta), v, 1)
         call Aprod (2,m,n,v,u,a,ja,na,leniw,lenrw,nabeg,naend)
         alpha  = dnrm2 (n, v, 1)
         if (alpha > zero) then
            call dscal (n, (one/alpha), v, 1)
         end if
      end if

!     ------------------------------------------------------------------
!     Use a plane rotation to eliminate the damping parameter.
!     This alters the diagonal (rhobar) of the lower-bidiagonal matrix.
!     ------------------------------------------------------------------
      rhbar1 = rhobar
      if (damped) then
         rhbar1 = d2norm( rhobar, damp )
         cs1    = rhobar/ rhbar1
         sn1    = damp  / rhbar1
         psi    = sn1 * phibar
         phibar = cs1 * phibar
      end if                   

!     ------------------------------------------------------------------
!     Use a plane rotation to eliminate the subdiagonal element (beta)
!     of the lower-bidiagonal matrix, giving an upper-bidiagonal matrix.
!     ------------------------------------------------------------------
      rho    =   d2norm( rhbar1, beta )
      cs     =   rhbar1/rho
      sn     =   beta  /rho
      theta  =   sn*alpha
      rhobar = - cs*alpha
      phi    =   cs*phibar
      phibar =   sn*phibar
      tau    =   sn*phi

!     ------------------------------------------------------------------
!     Update  x, w  and (perhaps) the standard error estimates.
!     ------------------------------------------------------------------
      t1     =   phi/rho
      t2     = - theta/rho
      t3     =   one/rho
      dknorm =   zero

      if (wantse) then
         do i=1,n
            t      = w(i)
            x(i)   = t1*t +  x(i)
            w(i)   = t2*t +  v(i)
            t      =(t3*t)**2
            se(i)  = t + se(i)
            dknorm = t + dknorm
         end do
      else
	 !$OMP PARALLEL private(i,t),REDUCTION(+:dknorm)
	 !$OMP DO
         do i=1,n
            t      = w(i)
            x(i)   = t1*t + x(i)
            w(i)   = t2*t + v(i)
            dknorm =(t3*t)**2 + dknorm
         end do
	 !$OMP END DO
	 !$OMP END PARALLEL
      end if

!     ------------------------------------------------------------------
!     Monitor the norm of d_k, the update to x.
!     dknorm = norm( d_k )
!     dnorm  = norm( D_k ),        where   D_k = (d_1, d_2, ..., d_k )
!     dxk    = norm( phi_k d_k ),  where new x = x_k + phi_k d_k.
!     ------------------------------------------------------------------
      dknorm = sqrt(dknorm)
      dnorm  = d2norm(dnorm,dknorm)
      dxk    = abs(phi*dknorm)
      if (dxmax < dxk) then
          dxmax   =  dxk
          maxdx   =  itn
      end if

!     ------------------------------------------------------------------
!     Use a plane rotation on the right to eliminate the
!     super-diagonal element (theta) of the upper-bidiagonal matrix.
!     Then use the result to estimate  norm(x).
!     ------------------------------------------------------------------
      delta  =   sn2*rho
      gambar = - cs2*rho
      rhs    =   phi - delta*z
      zbar   =   rhs/gambar
      xnorm  =   d2norm(xnorm1,zbar)
      gamma  =   d2norm(gambar,theta)
      cs2    =   gambar/gamma
      sn2    =   theta /gamma
      z      =   rhs   /gamma
      xnorm1 =   d2norm(xnorm1,z)

!     ------------------------------------------------------------------
!     Test for convergence.
!     First, estimate the norm and condition of the matrix  Abar,
!     and the norms of  rbar  and  Abar(transpose)*rbar.
!     ------------------------------------------------------------------
      Acond  = Anorm*dnorm
      res2   = d2norm(res2,psi)
      rnorm  = d2norm(res2,phibar)
      Arnorm = alpha*abs( tau )

!     Now use these norms to estimate certain other quantities,
!     some of which will be small near a solution.

      alfopt = sqrt( rnorm/(dnorm*xnorm) )
      test1  = rnorm/bnorm
      test2  = zero
      if (rnorm > zero) test2 = Arnorm/(Anorm*rnorm)
      test3  = one/Acond
      t1     = test1/(one + Anorm*xnorm/bnorm)
      rtol   = btol  + atol*Anorm*xnorm/bnorm

!     The following tests guard against extremely small values of
!     atol, btol  or  ctol.  (The user may have set any or all of
!     the parameters  atol, btol, conlim  to zero.)
!     The effect is equivalent to the normal tests using
!     atol = relpr,  btol = relpr,  conlim = 1/relpr.

      t3 = one + test3
      t2 = one + test2
      t1 = one + t1
      if (itn >= itnlim) istop = 5
      if (t3  <= one   ) istop = 4
      if (t2  <= one   ) istop = 2
      if (t1  <= one   ) istop = 1

!     Allow for tolerances set by the user.

      if (test3 <= ctol) istop = 4
      if (test2 <= atol) istop = 2
      if (test1 <= rtol) istop = 1

!     ------------------------------------------------------------------
!     See if it is time to print something.
!     ------------------------------------------------------------------
      prnt = .false.
      if (nout > 0) then
         if (n     <=        40) prnt = .true.
         if (itn   <=        10) prnt = .true.
         if (itn   >= itnlim-10) prnt = .true.
         if (mod(itn,10)  ==  0) prnt = .true.
         if (test3 <=  2.0*ctol) prnt = .true.
         if (test2 <= 10.0*atol) prnt = .true.
         if (test1 <= 10.0*rtol) prnt = .true.
         if (istop /=         0) prnt = .true.

         if (prnt) then   ! Print a line for this iteration.
            write(nout,1500) itn,x(1),rnorm,test1,test2,Anorm,Acond,alfopt
         end if
      end if

!     ------------------------------------------------------------------
!     Stop if appropriate.
!     The convergence criteria are required to be met on  nconv
!     consecutive iterations, where  nconv  is set below.
!     Suggested value:  nconv = 1, 2  or  3.
!     ------------------------------------------------------------------
      if (istop == 0) then
         nstop = 0
      else
         nconv = 1
         nstop = nstop + 1
         if (nstop < nconv  .and.  itn < itnlim) istop = 0
      end if
      if (istop == 0) go to 100

!     ==================================================================
!     End of iteration loop.
!     ==================================================================

!     Finish off the standard error estimates.

      if (wantse) then
         t = one
         if (m > n)  t = m-n
         if (damped) t = m
         t = rnorm/sqrt(t)
      
         do i=1,n
            se(i) = t*sqrt(se(i))
         end do
      end if

!     Decide if istop = 2 or 3.
!     Print the stopping condition.

  800 if (damped .and. istop==2) istop=3
      if (nout > 0) then
         write(nout, 2000)              &
            exitt,istop,itn,            &
            exitt,Anorm,Acond,          &
            exitt,bnorm, xnorm,         &
            exitt,rnorm,Arnorm
         write(nout, 2100)              &
            exitt,dxmax, maxdx,         &
            exitt,dxmax/(xnorm+1.0d-20)
         write(nout, 3000)              &
            exitt, msg(istop)
      end if

  900 return

!     ------------------------------------------------------------------
 1000 format(// 1p, a, '     Least-squares solution of  Ax = b'   &
      / ' The matrix  A  has', i7, ' rows   and', i7, ' columns'  &
      / ' damp   =', e22.14, 3x,        'wantse =', l10           &
      / ' atol   =', e10.2, 15x,        'conlim =', e10.2         &
      / ' btol   =', e10.2, 15x,        'itnlim =', i10)
 1200 format(// '   Itn       x(1)           Function',           &
      '     Compatible   LS        Norm A    Cond A')
 1300 format(// '   Itn       x(1)           Function',           &
      '     Compatible   LS     Norm Abar Cond Abar alfa_opt')
 1500 format(1p, i6, 2e17.9, 4e10.2, e9.1)
 2000 format(/ 1p, a, 5x, 'istop  =', i2,   15x, 'itn    =', i8   &
      /     a, 5x, 'Anorm  =', e12.5, 5x, 'Acond  =', e12.5       &
      /     a, 5x, 'bnorm  =', e12.5, 5x, 'xnorm  =', e12.5       &
      /     a, 5x, 'rnorm  =', e12.5, 5x, 'Arnorm =', e12.5)
 2100 format(1p, a, 5x, 'max dx =', e8.1 , ' occurred at itn ', i8 &
      /     a, 5x, '       =', e8.1 , '*xnorm')
 3000 format(a, 5x, a)

end subroutine LSQR

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine Aprod(mode,m,n,x,y,a,ja,na,leniw,lenrw,nabeg,naend)
implicit none
integer(kind=8)::lenrw,l,l1,l2,nabeg(leniw),naend(leniw)
integer::mode,m,n,leniw
integer::ja(lenrw),na(leniw)
double precision::x(n),y(m),a(lenrw)
double precision::sum1
integer::i,j
real*8::t1,t2
integer::maptoray(lenrw)
double precision::val(lenrw)

if(leniw.gt.m)then
    write(*,*)"message1 from Aprod error!"
    stop
endif
!if(lenrw.gt.m*n)then
!    write(*,*)"message2 from Aprod error!"
!    stop
!endif

l2=0
if(mode.eq.1)then
 call cputime(t1)
    !$OMP PARALLEL private(i,sum1,l1,l2,l,j) num_threads(10)
    !$OMP DO
    do i=1,leniw
	sum1=0.0
	l1=nabeg(i);l2=naend(i)
	do l=l1,l2
	    j=ja(l)
	    sum1=sum1+a(l)*x(j)
	enddo
	y(i)=y(i)+sum1
    enddo
    !$OMP END DO
    !$OMP END PARALLEL
 call cputime(t2)
 write(*,*)"CPUTIME for       matrix-vector multiplication:",t2-t1;
elseif(mode.eq.2)then
 call cputime(t1)
    !$OMP PARALLEL private(i,l1,l2,l,j) num_threads(10) REDUCTION(+:x)
    !$OMP DO
    do i=1,leniw
	l1=nabeg(i);l2=naend(i)
	do l=l1,l2
	    j=ja(l)
	    x(j)=x(j)+a(l)*y(i)
	enddo
    enddo
    !$OMP END DO
    !$OMP END PARALLEL
 call cputime(t2)
 write(*,*)"CPUTIME for trans-matrix-vector multiplication:",t2-t1;
endif
end subroutine Aprod
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine  dcopy(n,dx,incx,dy,incy)
implicit none
double precision::dx(n),dy(n)
integer::i,incx,incy,ix,iy,m,n

if(n.le.0)then
    return
endif

if(incx.eq.1.and.incy.eq.1)then
    m=mod(n,7)
    if(m.ne.0)then
	dy(1:m)=dx(1:m)
    endif

    do i=m+1,n,7
	dy(i)=dx(i)
	dy(i+1)=dx(i+1)
	dy(i+2)=dx(i+2)
	dy(i+3)=dx(i+3)
	dy(i+4)=dx(i+4)
	dy(i+5)=dx(i+5)
	dy(i+6)=dx(i+6)
    enddo

else
    if(0.le.incx)then
	ix=1
    else
	ix=(-n+1)*incx+1
    endif
    if(0.le.incy)then
	iy=1
    else
	iy=(-n+1)*incy+1
    endif
    do i=1,n
	dy(iy)=dx(ix)
	ix=ix+incx
	iy=iy+incy
    enddo
endif
end subroutine dcopy
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine  dscal(n,sa,x,incx)
implicit none
integer::i,incx,ix,m,n
double precision::sa
double precision::x(n)
if(n.le.0)then
    return
elseif(incx.eq.1)then
    m=mod(n,5)
    x(1:m)=sa*x(1:m)
    !$OMP PARALLEL private(i)
    !$OMP DO
    do i=m+1,n,5
	x(i)  =sa*x(i)
	x(i+1)=sa*x(i+1)
	x(i+2)=sa*x(i+2)
	x(i+3)=sa*x(i+3)
	x(i+4)=sa*x(i+4)
    enddo
    !$OMP END DO
    !$OMP END PARALLEL
else
    if(0.le.incx ) then
	ix=1
    else
	ix=(-n+1)*incx+1
    endif
    do i=1,n
	x(ix)=sa*x(ix)
	ix=ix+incx
    enddo
endif
end subroutine dscal
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
double precision function dnrm2(n,x,incx)
implicit none
integer::ix,n,incx,endx
double precision::x(n),ssq,absxi,norm,scale

if(n.lt.1.or.incx.lt.1)then
    norm=0.0
elseif(n.eq.1)then
    norm=abs(x(1))
else
    scale=0.0
    ssq=1.0
    endx=1+(n-1)*incx
    do ix=1,endx,incx
	if(x(ix).ne.0.0)then
	    absxi=abs(x(ix))
	    if(scale.lt.absxi)then
		ssq=1.0+ssq*(scale/absxi)**2
		scale=absxi
	    else
		ssq=ssq+(absxi/scale)**2
	    endif
	endif
    enddo
    norm=scale*sqrt(ssq)
endif
dnrm2=norm
end function dnrm2
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
double precision function dnrm2new(n,x,incx)
implicit none
integer n, incx
double precision x(n)
!c     **********
!c
!c     Function dnrm2
!c
!c     Given a vector x of length n, this function calculates the
!c     Euclidean norm of x with stride incx.
!c
!c     The function statement is
!c
!c       double precision function dnrm2(n,x,incx)
!c
!c     where
!c
!c       n is an integer variable.
!c         On entry n is the length of x.
!c         On exit n is unchanged.
!c
!c       x is a double precision array of dimension n.
!c         On entry x specifies the vector x.
!c         On exit x is unchanged.
!c
!c       incx is an integer variable.
!c         On entry incx specifies the stride.
!c         On exit incx is unchanged.
!c
!c     MINPACK-2 Project. October 1993.
!c     Argonne National Laboratory and University of Minnesota.
!c     Brett M. Averick and Jorge J. More'.
!c
!c     **********
double precision zero
parameter (zero=0.0d0)
integer i
double precision scale,norm

scale = zero
!$OMP PARALLEL private(i),REDUCTION(MAX:scale),if(n>5000)
!$OMP DO
do i = 1, n, incx
    scale = max(scale,abs(x(i)))
enddo
!$OMP END DO
!$OMP END PARALLEL

norm = zero
if (scale .eq. zero) return

!$OMP PARALLEL private(i),REDUCTION(+:norm),if(n>5000)
!$OMP DO
do i = 1, n, incx
    norm = norm + (x(i)/scale)**2
enddo
!$OMP END DO
!$OMP END PARALLEL

dnrm2new = scale*sqrt(norm)
end function dnrm2new
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
double precision function d2norm(a,b)
implicit none
double precision::a,b
double precision::scale
double precision::zero=0.0

scale=abs(a)+abs(b)
if(scale.eq.zero)then
    d2norm=zero
else
    d2norm=scale*sqrt((a/scale)**2+(b/scale)**2)
endif
end function d2norm
