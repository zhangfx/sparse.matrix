program inver
implicit none
character::vrs*8,velfile*180,dampargv*60
character::mark*160,gfile*180,velfileout*180,residualfile*180,dampfile*180
character::temchar*1
character::stringline*300
integer::countfield,fieldsw
integer::i,j,k,l
real::damp,rdum
real::lon0,lons,lon1,lat0,lats,lat1,dep0,deps,dep1
integer::lonn,latn,depn
integer::nvolu,clon,clat,cdep,temint
real,allocatable::velin(:),velout(:),inverx(:),modelvararray(:),velrefer(:)
real,allocatable::lon(:),lat(:),dep(:)

integer::raynum,receivernum,sourcenum
integer::ncoef
integer,allocatable::jdx(:)
real,allocatable::coef(:),res(:)
real,parameter::fractor=0.7
integer(kind=8)::maxf,temintverylon
integer(kind=8),allocatable::nabeg(:),naend(:)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
integer,allocatable::na(:),ja(:)
double precision,allocatable::a(:),x(:),b(:)
double precision::atol,btol,conlim,dampsub
logical::wantse
integer::itnlim,istop,itn
double precision,allocatable::u(:),se(:)
double precision::Anorm,Acond,rnorm,Arnorm,xnorm
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
real,allocatable::res2(:)
real::varfunc,residualvar,modelvar
real*8::t1,t2

if(iargc().lt.3)then
    write(*,*)"Usage:inver Gfile Gfile_based_velfile damp.float"
    stop
endif

call getarg(1,gfile)
call getarg(2,velfile)
vrs='ratio'
if(vrs.eq.'vel')then
elseif(vrs.eq.'ratio')then
elseif(vrs.eq.'slow')then
elseif(vrs.eq.'local')then
else
    write(*,*)"Message from inver: vel|ratio|slow|local option is error"
    stop
endif

if(gfile(2:2).ne.vrs(1:1))then
    write(*,*)"Message from inver: the Gfile is not compatible with vel|ratio|slow|local "
    stop
endif
mark=gfile(index(gfile,'_')+1:len_trim(gfile))

open(unit=10,file=velfile,status='old')
read(10,*)temchar,lon0,lons,lonn,lon1
read(10,*)temchar,lat0,lats,latn,lat1
read(10,*)temchar,dep0,deps,depn,dep1

read(10,'(A)')stringline
backspace(10)
fieldsw=countfield(stringline)
if(fieldsw.eq.4 .or. fieldsw.eq.5)then
else
    write(*,*)"Please check grid.velocity";
    stop
endif

nvolu=lonn*latn*depn
allocate(velin(nvolu),velout(nvolu),inverx(nvolu),modelvararray(nvolu),velrefer(nvolu))
allocate(lon(lonn),lat(latn),dep(depn))
do i=1,lonn
    clon=i
    do j=1,latn
	clat=(j-1)*lonn
	do k=1,depn
	    cdep=(k-1)*lonn*latn
	    temint=clon+clat+cdep
	    if(fieldsw.eq.4)then
		read(10,*)rdum,rdum,rdum,velin(temint)
		velrefer(temint)=velin(temint)
	    endif
	    if(fieldsw.eq.5)then
		read(10,*)rdum,rdum,rdum,velin(temint),velrefer(temint)
	    endif
	enddo
    enddo
enddo
close(10)
do i=1,lonn
    lon(i)=lon0+(i-1)*lons
enddo
do j=1,latn
    lat(j)=lat0+(j-1)*lats
enddo
do k=1,depn
    dep(k)=dep0+(k-1)*deps
enddo

open(unit=20,file=gfile,form='unformatted',status='old')
read(20)raynum,lonn,latn,depn,receivernum,sourcenum
allocate(jdx(nvolu),coef(nvolu),res(raynum))
! kind=4, 2^31-1 = 2147483647         (default)
! kind=8, 2^63-1 = 9223372036854775807
maxf=nint(4.0e8,kind=8)

temintverylon=0
allocate(nabeg(raynum),naend(raynum))
allocate(na(raynum),ja(maxf),a(maxf),x(nvolu),b(raynum))
allocate(u(raynum),se(nvolu))
do i=1,raynum
    read(20)ncoef,(jdx(k),coef(k),k=1,ncoef),res(i)
    na(i)=ncoef
    if(i.eq.1)then
	nabeg(i)=1
	naend(i)=ncoef
    else
	nabeg(i)=naend(i-1)+1
	naend(i)=naend(i-1)+ncoef
    endif
    if(naend(i).gt.maxf)then
	write(*,*)"Message from inver: please increase the maxf"
	stop
    endif
    do j=1,ncoef
	temintverylon=temintverylon+1
	ja(temintverylon)=jdx(j)
	a(temintverylon)=coef(j)
    enddo
    b(i)=res(i)
    !if(mod(i,10000).eq.0)write(*,*)i,"of",raynum
enddo
close(20)
maxf=temintverylon
write(*,*)maxf

allocate(res2(raynum))
l=2
100 continue
l=l+1
call getarg(l,dampargv);read(dampargv,*)damp
atol=0.0
btol=0.0
conlim=0.0
dampsub=damp
wantse=.false.
u=b
itnlim=3000

 call cputime(t1)
 call LSQR(raynum,nvolu,dampsub,wantse,a,ja,na,raynum,maxf, &
           u,x,se,nabeg,naend,		       &
	   atol,btol,conlim,itnlim,	       &
 	   istop,itn,Anorm,Acond,rnorm,Arnorm,xnorm)
 call cputime(t2)
 write(*,*)"CPUTIME for LSQR subroutine:",t2-t1;

do i=1,nvolu
    inverx(i)=x(i)
    if(vrs.eq."vel"  )velout(i)=velin(i)+inverx(i)
    if(vrs.eq."ratio")velout(i)=velin(i)+velin(i)*inverx(i)
    if(vrs.eq."slow" )velout(i)=velin(i)-velin(i)**2*inverx(i)
    modelvararray(i)=inverx(i)
    if(vrs.eq."local")then
	if(inverx(i).le.0.01)then
	    velout(i)=velin(i)
	else
	    velout(i)=1.0/inverx(i)
	endif
	modelvararray(i)=velout(i)-velin(i)
    endif
enddo


velfileout="try."//mark(1:len_trim(mark))//"."//dampargv(1:len_trim(dampargv))//"."//vrs(1:len_trim(vrs))
open(unit=80,file=velfileout)
write(80,fmt="('#',$)")
write(80,*)lon0,lons,lonn,lon1
write(80,fmt="('#',$)")
write(80,*)lat0,lats,latn,lat1
write(80,fmt="('#',$)")
write(80,*)dep0,deps,depn,dep1
do i=1,lonn
    clon=i
    do j=1,latn
	clat=(j-1)*lonn
	do k=1,depn
	    cdep=(k-1)*lonn*latn
	    temint=clon+clat+cdep
	    write(80,*)lon(i),lat(j),dep(k),velout(temint),velin(temint),velrefer(temint),inverx(temint)
	enddo
    enddo
enddo
close(80)


if(l<iargc())goto 100
deallocate(velin,velout,inverx,modelvararray,velrefer,lon,lat,dep)
deallocate(jdx,coef,res)
deallocate(nabeg,naend)
deallocate(na,ja,a,x,b,u,se)
deallocate(res2)
end
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
real function varfunc(in,num)
implicit none
integer::i,num
real::in(num)
varfunc=0.0
do i=1,num
    varfunc=varfunc+in(i)**2
enddo
varfunc=varfunc/real(num)
end function varfunc
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!
integer function countfield(stringline)
implicit none
character::stringline*300,charfield*1
integer::i,j,io

countfield=0
do i=1,20
    read(stringline,*,iostat=io) (charfield,j=1,i)
    if(io.eq.-1)then
	countfield=i-1
	exit
    endif
enddo

end function countfield
